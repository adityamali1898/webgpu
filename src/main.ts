import {APP} from "./control/app";

const canvas : HTMLCanvasElement =  <HTMLCanvasElement> document.createElement('canvas');

const app = new APP(canvas);
app.run()