import { mat4 } from "gl-matrix";
import { Quad } from "./quad";
import { RenderData, object_types } from "./definitions";


export class Scene {

    quads: Quad[];
    object_data: Float32Array;
    quad_count: number;

     constructor() {

        this.quads = [];
        this.object_data = new Float32Array(16 * 1024);
        this.quad_count = 0;

        this.make_quads();
    }

    make_quads() {
        var i: number = 5;
        for (var x: number = -10; x <= 10; x++) {
            for (var y:number = -10; y <= 10; y++) {
                this.quads.push(
                    new Quad(
                        [x, y, 0]
                    )
                );

                console.log(i,x,y)

                var blank_matrix = mat4.create();
                for (var j: number = 0; j < 16; j++) {
                    this.object_data[16 * i + j] = <number>blank_matrix.at(j);
                }
                i++;
                this.quad_count++;
            }
        }
    }

     update() {
        var i: number = 0;

        this.quads.forEach(
            (quad) => {
                quad.update();
                var model = quad.get_model();
                for (var j: number = 0; j < 16; j++) {
                    this.object_data[16 * i + j] = <number>model.at(j);
                }
                i++;
            }
        );

     }

     get_renderables(): RenderData {
        return {
            view_transform: mat4.create(),
            model_transforms: this.object_data,
            object_counts: {
                [object_types.QUAD]: this.quad_count,
            }
        }
    }
    



}
