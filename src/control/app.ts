import { Renderer } from "../view/renderer";

export class APP{
    canvas: HTMLCanvasElement;
    renderer: Renderer;

    constructor(canvas: HTMLCanvasElement)
    {
        this.canvas=canvas;
        this.renderer=new Renderer(canvas);
        this.renderer.initialize();

    }


    // Game Loop
    run =() => {

        if(this.renderer.adapter)
        {
            console.log("Supports GPU")
        }
        

    }




}