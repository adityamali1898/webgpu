import { mat4, vec3 } from "gl-matrix"
import { QuadMesh } from "./quad_mesh"
import { RenderData, object_types } from "../model/definitions"
import shader from "./shaders/shader.wgsl"

export class Renderer {
  canvas: HTMLCanvasElement

  // device context objects
  adapter: GPUAdapter
  device: GPUDevice
  context: GPUCanvasContext
  format: GPUTextureFormat
  quad: QuadMesh
  uniformBuffer: GPUBuffer
  

  
  
  quadBuffer: GPUBuffer
  quadBufferLayout: GPUVertexBufferLayout
  pipeline: GPURenderPipeline
  frameGroupLayout: GPUBindGroupLayout
  frameBindGroup: GPUBindGroup


  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas
    this.quad= new QuadMesh(this.device)
  }

  async initialize(): Promise<void> {
    
    await this.setupDevice()
    
    await this.makeBindGroupLayout();

    await this.createAsset();

    await this.createModelViewProjection();

    await this.makeDepthBufferResources();
    
    await this.makePipeline();

    await this.makeBindGroup();


  }
  async createModelViewProjection() {

    this.uniformBuffer =  this.device.createBuffer({
      size: 64 *2,
      usage: GPUBufferUsage.UNIFORM |GPUBufferUsage.COPY_DST
    })

  }

  async makeBindGroup() {
    this.frameBindGroup = this.device.createBindGroup({
            layout: this.frameGroupLayout,
            entries: [
                {
                    binding: 0,
                    resource: {
                        buffer: this.uniformBuffer
                    }
                }
            ]
        });
  }

  async makePipeline() {

        
        const pipelineLayout = this.device.createPipelineLayout({
            bindGroupLayouts: [this.frameGroupLayout]
        });
   
    this.pipeline =  this.device.createRenderPipeline({
      vertex:{
        module: this.device.createShaderModule({
          code: shader
        }),
        entryPoint:'vs_main',
        buffers:[this.quadBufferLayout]
      },
      fragment:{
        module:this.device.createShaderModule({
          code: shader
        }),
        entryPoint:'fs_main',
        targets:[{
          format:this.format
        }]

      },

      primitive:{topology:"triangle-list"},
      layout:pipelineLayout,
    })

  }

  async makeDepthBufferResources() {
    throw new Error("Method not implemented.")
  }

  async createAsset() {

    // QUAD 
     
    this.quadBufferLayout = {
      arrayStride: 5*4,
      attributes:[
        {
          shaderLocation:0,
          format:"float32x3",
          offset:0
        },
        {
          shaderLocation:0,
          format:"float32x2",
          offset:12
        }
      ]
    }


    // create buffer
    this.quadBuffer =  this.device.createBuffer({
      size: this.quad.vertices.byteLength,
      usage: GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST,
      mappedAtCreation:true
    })

    // load vertices in buffer
    new Float32Array(this.quadBuffer.getMappedRange()).set(this.quad.vertices) 
    this.quadBuffer.unmap();

    // Quad End


  }

  async makeBindGroupLayout() {
   

        this.frameGroupLayout = this.device.createBindGroupLayout({
            entries: [
                {
                    binding: 0,
                    visibility: GPUShaderStage.VERTEX,
                    buffer: {}
                },
             
            ]

        });
    
    
  }

  async setupDevice(): Promise<void> {
    
    this.adapter = <GPUAdapter> await navigator.gpu?.requestAdapter();
    this.device = <GPUDevice> await this.adapter?.requestDevice();
    this.device = <GPUDevice> await this.adapter?.requestDevice();
         if (!this.device) {
          console.log('need a browser that supports WebGPU');
          return;
          }
          else{
            console.log("supports webgpu")
          }
    this.context = <GPUCanvasContext> await this.canvas.getContext("webgpu")
    this.format = <GPUTextureFormat> navigator.gpu?.getPreferredCanvasFormat();

    // cofigure context
    this.context.configure({
        device: this.device,
        format: this.format,
        alphaMode: "opaque"
    })


  }

  async render( renderables: RenderData){
    const projection = mat4.create();
    const view = mat4.create();
    const eye: vec3 = <vec3> [-500, 300, -500];
    const target : vec3 = <vec3>  [0, -100, 0];
    const up : vec3 = <vec3>  [0, 1, 0];
    mat4.perspective(projection, Math.PI/4, 800/600, 0.1, 10);
    mat4.lookAt(view, eye, target,up);
    

    this.device.queue.writeBuffer(this.uniformBuffer,0,<ArrayBuffer> view)
    this.device.queue.writeBuffer(this.uniformBuffer, 64, <ArrayBuffer>projection); 
    

    const commandEncoder : GPUCommandEncoder = this.device.createCommandEncoder();
    const textureView: GPUTextureView = this.context.getCurrentTexture().createView();
    const renderpass: GPURenderPassEncoder = commandEncoder.beginRenderPass({
      colorAttachments:[{
        view:textureView,
        clearValue: {r: 0.5, g: 0.0, b: 0.25, a: 1.0},
        loadOp: "clear",
        storeOp: "store"
      }]

    });

    var objects_drawn: number = 0;

    renderpass.setPipeline(this.pipeline);
    renderpass.setBindGroup(0, this.frameBindGroup)
    renderpass.setVertexBuffer(0,this.quadBuffer)
    renderpass.draw( 6, renderables.object_counts[object_types.QUAD], 0, objects_drawn)
    objects_drawn += renderables.object_counts[object_types.QUAD];
    renderpass.end();
    
    this.device.queue.submit([commandEncoder.finish()]);

  }
}