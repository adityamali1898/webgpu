export class QuadMesh {

    buffer: GPUBuffer
    bufferLayout: GPUVertexBufferLayout
    vertices: Float32Array

    constructor(device: GPUDevice) {

        // x y z u v
         this.vertices  = new Float32Array(
            [
                -0.5, -0.5, 0.0, 0.0, 0.0,
                 0.5, -0.5, 0.0, 1.0, 0.0,
                 0.5,  0.5, 0.0, 1.0, 1.0,

                 0.5,  0.5, 0.0, 1.0, 1.0,
                -0.5,  0.5, 0.0, 0.0, 1.0,
                -0.5, -0.5, 0.0, 0.0, 0.0
            ]
        );

        }
}